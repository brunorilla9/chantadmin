package com.example.chantadmin

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView



class MainFragment : Fragment() {

    private lateinit var userNameText: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // find the element
        Log.i("FRAGMENT SUCCEEDED", "FRAGMENT")

        val userNameText = view.findViewById<TextView>(R.id.login_userName)
        Log.i("MESA", userNameText.text.toString())
        /*
        val bundle = arguments
        val message = bundle!!.getString("userNameText")

        userNameText.text = message
        */

        /*
         val counter = view.findViewById<TextView>(R.id.counter)

         // set onClick Listener to a button
         view.findViewById<Button>(R.id.plus).setOnClickListener {
             var counterValue = counter.text.toString().toInt()
             counter.text = (++counterValue).toString()
         }

         view.findViewById<Button>(R.id.minus).setOnClickListener {
             var counterValue = counter.text.toString().toInt()
             if(counterValue > 0 ) counter.text = (--counterValue).toString()
         }

         */
    }


}