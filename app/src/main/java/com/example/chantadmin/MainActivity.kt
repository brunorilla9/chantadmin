package com.example.chantadmin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import android.widget.EditText
import android.content.Intent
import com.google.android.material.textfield.TextInputEditText

const val FULL_NAME_KEY = "FULL_NAME_KEY"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setear onClick listener para responder al click de botón y devolver formData
        findViewById<Button>(R.id.enter_button)?.setOnClickListener {
            // Obtener el texto de saludo
            val greetingDisplay = findViewById<TextView>(R.id.greeting_display)

            // obtener otro texto
            val userName = findViewById<TextInputEditText>(R.id.username)?.text.toString().trim()
            val password = findViewById<TextInputEditText>(R.id.password)?.text.toString().trim()

            // me fijo si los inputs no están vacíos

            if (userName.isNotEmpty() && password.isNotEmpty()) {
                greetingDisplay?.text = getString(R.string.welcome_to_the_app)

                // Lanzar mediante intent la nueva activity (Logged in screen)
                Intent(this, LoggedInSreen::class.java)
                    .also { welcomeIntent ->
                        welcomeIntent.putExtra(FULL_NAME_KEY, userName)
                        // Launch
                        startActivity(welcomeIntent)
                    }


            } else {
                Toast.makeText(this, getString(R.string.enter_values), Toast.LENGTH_LONG).apply {
                    setGravity(
                        Gravity.CENTER, 0, 0
                    )
                    show()
                }
            }

        }

    }
}