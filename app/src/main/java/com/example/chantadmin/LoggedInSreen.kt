package com.example.chantadmin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class LoggedInSreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_logged_in_sreen)

        // handler del intent que viene de Main Activity
        intent?.let {
            val userName = it.getStringExtra(FULL_NAME_KEY)
            Log.i("USER NAME DESDE INTENT", userName.toString()
            )
            val mFragmentManager = supportFragmentManager
            val mFragmentTransaction = mFragmentManager.beginTransaction()
            val mFragment = MainFragment()

            val mBundle = Bundle()
            mBundle.putString("mText", userName.toString())
            mFragment.arguments = mBundle
            mFragmentTransaction.add(R.id.fragment_main, mFragment).commit()
        }



    }
}